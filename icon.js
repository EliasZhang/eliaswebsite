import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart, faTimesCircle, faExclamationCircle, faEnvelope, faDownload } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

export default function addIcon() {
  library.add(faHeart, faTimesCircle, faExclamationCircle, faEnvelope, faDownload)
  Vue.component('font-awesome-icon', FontAwesomeIcon)
}
