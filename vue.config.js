module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/' + process.env.CI_PROJECT_NAME + '/' : '/',
  chainWebpack: config => {
    config.module
      .rule('documents')
      .test(/\.pdf$/)
      .use('file-loader')
      .loader('file-loader')
      .end()
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
  }
}
